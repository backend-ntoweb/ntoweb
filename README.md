# ntoweb

Quando un progetto con sottomoduli viene clonato usando git clone,
crea le directory che contengono sottomoduli, ma nessuno dei file al loro interno.

Eseguire i seguenti comandi: 

```
git clone https://gitlab.com/backend-ntoweb/ntoweb.git
cd ntoweb
git submodule init
git submodule update
```

oppure: 

```
git clone --recurse-submodules https://gitlab.com/backend-ntoweb/ntoweb.git
```